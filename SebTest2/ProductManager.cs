﻿using Models;
using SebTest2.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SebTest2
{
    public interface IProductManager {
        IList<Bundle> ResolveRuleFromQuestionResults(Question question);
    }


    public class ProductManager: IProductManager
    {
        private IBaseRepository<Bundle> _bundleRepository;
        public ProductManager() {
            _bundleRepository = new BundleRepository();
        }

        public IList<Bundle> ResolveRuleFromQuestionResults(Question question)
        {
            var bundles = _bundleRepository.GetList();
            var questionRules = question.GetRules();
            var rules = bundles.Where(x=> x.Rules.Any(r=> questionRules.Any(q=> GetDifferences(r, q)))).ToList();

            return rules;
        }

        private bool GetDifferences(Rule test1, Rule test2)
        {
            if (test1.Action == test2.Action
                && test1.IntValue == test2.IntValue
                && test1.BoolValue == test2.BoolValue
                && test1.Type == test2.Type
                )
                return true;

            return false;
        }
    }
}
