﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SebTest2.Repositories
{
    public class ProductRepository: IBaseRepository<Products>
    {
        private IList<Products> _products;
        public ProductRepository() {
            _products = new List<Products>();
            InitList();
        }
        
        public void InitList()
        {
            _products = new List<Products>(){
                new Products() {
                    Id = 1,
                    Name = "Current Account",
                    Rules = new List<Rule>()
                    {
                        new Rule() { Action = LogicalAction.MoreThan, IntValue = 0, Type = QuestionTypes.Income },
                        new Rule() { Action = LogicalAction.MoreThan, IntValue = 17, Type = QuestionTypes.Age }
                    }
                },
                new Products(){
                    Id = 2,
                    Name = "Junior Savior Account",
                    Rules = new List<Rule>()
                    {
                        new Rule(){ Action = LogicalAction.LessThan, IntValue = 18, Type = QuestionTypes.Age }
                    }
                },
                new Products(){
                    Id = 3,
                    Name = "Student Account",
                    Rules = new List<Rule>()
                    {
                        new Rule(){ Action = LogicalAction.MoreThan, IntValue = 17, Type = QuestionTypes.Age },
                        new Rule(){ Action = LogicalAction.Equals, BoolValue = true ,Type = QuestionTypes.Student }
                    }
                },
                new Products(){
                    Id = 4,
                    Name = "Debit Card",
                    Rules = new List<Rule>()
                    {
                        new Rule(){ MustBeIncludedIn = new int[] {1, 2, 3  }  }
                    }
                }
            };
        }

        public IList<Products> GetList() {
            //InitList();
            return _products;
        }
    }
}
