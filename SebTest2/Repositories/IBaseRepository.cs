﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SebTest2.Repositories
{
    public interface IBaseRepository<T> where T : class
    {
        IList<T> GetList();
    }
}
