﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SebTest2.Repositories
{
    public class BundleRepository : IBaseRepository<Bundle>
    {
        private IList<Bundle> _bundles;
        public BundleRepository() {
            _bundles = new List<Bundle>();
            InitList();
        }

        public void InitList() {
            _bundles = new List<Bundle>() {
                new Bundle()
                {
                    Id = 1,
                    Label = "Junior Saver",
                    IncludedProducts = new List<Products>() { new Products() { Id = 2, Name = "Junior Saver Account", Rules = new List<Rule>() { new Rule() { Action = LogicalAction.LessThan, IntValue = 18, Type = QuestionTypes.Age } } } },
                    Rules = new List<Rule>(){ new Rule() { Action = LogicalAction.LessThan, IntValue = 18, Type = QuestionTypes.Age } },
                    Rate = 0
                },
                new Bundle()
                {
                    Id = 2,
                    Label = "Student",
                    IncludedProducts = new List<Products>(){ },
                    Rules = new List<Rule>(){ new Rule() { Action = LogicalAction.MoreThan, IntValue = 0, Type = QuestionTypes.Age } },
                    Rate = 0
                }
            };
        }

        public IList<Bundle> GetList()
        {
            return _bundles;
        }
    }
}
