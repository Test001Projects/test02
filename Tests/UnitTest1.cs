﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SebTest2;
using Models;

namespace Tests
{
    [TestClass]
    public class UnitTest1
    {
        private IProductManager _prodManager;

        [TestInitialize]
        public void Setup()
        {
            Resolver.Main();
            _prodManager = new ProductManager();
        }

        [TestMethod]
        public void TestMethod1()
        {
            var question = new Question() { Age = 17, Income = 0 };
            var smth = _prodManager.ResolveRuleFromQuestionResults(question);

            Assert.IsTrue(smth.Count > 0);
        }
    }
}
