﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Products : BaseModel
    {
        public string Name { get; set; }
        public IList<Rule> Rules { get; set; }
    }

    public class Rule: BaseModel
    {
        public LogicalAction Action { get; set; }
        public string Label { get; set; }
        public QuestionTypes Type { get; set; }
        public int IntValue { get; set; }
        public bool BoolValue { get; set; }
        public int[] MustBeIncludedIn { get; set; }
    }

    public enum LogicalAction : int
    {
        MoreThan = 1,
        LessThan = 2,
        Equals = 3
    }

    public enum QuestionTypes : int
    {
        Age = 0,
        Student = 1,
        Income = 2
    }
}
