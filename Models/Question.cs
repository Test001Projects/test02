﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Question
    {
        public int? Age { get; set; }
        public bool? Student { get; set; }
        public int? Income { get; set; }

        public List<Rule> GetRules()
        {
            var rules = new List<Rule>();
            if (Age.HasValue)
            {
                LogicalAction action = (Age.Value == 17) ? LogicalAction.LessThan : LogicalAction.MoreThan;
                int ageValue = Age.Value == 17 ? 18 : Age.Value;
                rules.Add(new Rule() { Type = QuestionTypes.Age, IntValue = ageValue, Action = action });
            }

            if (Student.HasValue)
                rules.Add(new Rule() { Type = QuestionTypes.Student, BoolValue = Student.Value, Action = LogicalAction.Equals });

            if (Income.HasValue)
            {
                var incomeValue =  Income.Value == 0? 0: Income.Value - 1;
                if (incomeValue > 0)
                    rules.Add(new Rule() { Type = QuestionTypes.Student, IntValue = incomeValue, Action = LogicalAction.MoreThan });
            }
                

            return rules;
        }
    }
}
