﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Bundle : BaseModel
    {
        public string Label { get; set; }
        public IList<Products> IncludedProducts { get; set; }
        public IList<Rule> Rules { get; set; }
        public int Rate { get; set; }
    }
}
